# Git Aliases

# artisan
alias pa='php artisan'
alias pas='php artisan serve'
alias migrate='php artisan migrate'
alias migration='php artisan make:migration'
alias model='php artisan make:model'
alias controller='php artisan make:controller'

# git
alias ga='git add .'
alias gcm='git commit -m'
alias pull='git pull'
alias push='git push'
alias gs='git status'

# composer
alias ci='composer install'
alias cu='composer update'
alias cr='composer require'
alias crm='composer remove'

# npm
alias n='npm'
alias ni='npm install'
alias nrd='npm run dev'
alias nrm='npm remove'

# windows
alias dev='cd /c/wamp/www/Projects'
alias c='code .'
alias cl='clear'
alias x='exit'
